/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./index.html",
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: '#111',
        secondary:'#b4a7d6',
        tertiary:'#555',
        light: '#333',
      }
    },
  },
  plugins: [],
}

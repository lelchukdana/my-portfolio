import BingeworthyImage from '../assets/images/BingeworthyLaptop.png';
import CarCarImage from '../assets/images/CarCarLaptop.gif';
import CarCarImageDark from '../assets/images/CarCarLaptopBlackBG.gif';
import RainOrShineImage from '../assets/images/RainOrShineLaptop.png';
import FutureProjectsImage from '../assets/images/FutureProjectsLaptop.png';
import FutureProjectsImageDark from '../assets/images/FutureProjectsLaptopBlack.png';

const projects = [
    {
        id: 1,
        name: 'Bingeworthy',
        description: 'Bingeworthy is a streaming project that I collaborated on which allows users to find streaming providers, explore, rate, review, comment, and save TV shows and Movies to a watchlist',
        image: BingeworthyImage,
        imageDarkMode: BingeworthyImage,
        link:'https://gitlab.com/team-13-pending/bingeworthy',
    },
    {
        id: 2,
        name: 'CarCar',
        description: 'CarCar is a an application managing all aspects of an automobile dealership, including inventory, service and sales.',
        image: CarCarImage,
        imageDarkMode: CarCarImageDark,
        link:'https://gitlab.com/_Erick.33/project-beta'
    },
    {
        id: 3,
        name: 'RainOrShine',
        description: 'RainOrShine is a an application which allows users to view weather data for any city',
        image: RainOrShineImage,
        imageDarkMode: RainOrShineImage,
        link:'https://gitlab.com/lelchukdana/rainorshine'
    },
    {
        id: 4,
        name: 'Coming Soon',
        description: 'I am working on more projects at this time! Please check back soon for updates',
        image: FutureProjectsImage,
        imageDarkMode: FutureProjectsImageDark,
        link:'https://gitlab.com/lelchukdana/bookbook'
    },
]


export default projects;

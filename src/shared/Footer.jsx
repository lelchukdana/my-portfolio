import React from 'react';
import linkedin from "../assets/icons/linkedin-white.png";
import gitlab from "../assets/icons/gitlab-white.png";
import github from "../assets/icons/github-white.png";

const Footer = () => {
  return (
    <div className='bg-black md:h-96 px-7'>
      <div className='max-w-7xl mx-auto py-20 flex flex-col md:flex-row justify-between'>
        <div className='md:w-2/5 my-3'>
          <h4 className='text-white font-bold text-2xl tracking-wide'>DANA LELCHUK</h4>
          <p className='mt-5 text-sm leading-7 text-[#eee]'>Full-Stack Software Developer</p>
        </div>
        <div className='my-3'>
          <h4 className='text-white font-bold text-2xl tracking-wide'>SOCIALS</h4>
          <div className='mt-5 flex gap-3'>
            <a href="https://www.linkedin.com/in/danalelchuk/" className='ml-1'><img src={linkedin} alt="" className='w-7 h-7'/></a>
            <a href="https://github.com/danalelchuk" className='ml-1'><img src={github} alt="" className='w-7 h-7'/></a>
            <a href="https://gitlab.com/lelchukdana" className='ml-1'><img src={gitlab} alt="" className='w-8 h-8'/></a>
          </div>
        </div>
      </div>
      <hr className='text-slate-50 opacity-30 px-7'/>
      <p className='text-white md:my-10 py-10 md:py-0 text-sm leading-7 text-center'>2023 Dana Lelchuk&copy;</p>
    </div>
  );
};

export default Footer;

/* eslint-disable no-unused-vars */
import React from 'react';

const Headline = ({title}) => {
    return (
        <div className='dark:text-gray-300 text-center mx-auto md:w-2/3 my-16'>
            <h2 className='text-4xl rubber-duck-font tracking-wide mt-24 mb-5'>{title}</h2>
            <div className='divider'></div>

        </div>
    );
};

export default Headline;


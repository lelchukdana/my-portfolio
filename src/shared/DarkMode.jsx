import React, {createContext, useState, useContext} from 'react';
// Create the context
const DarkModeContext = createContext();

// Create a provider to wrap the components that need access to the dark mode state
export const DarkModeProvider = ({ children }) => {
  const [darkMode, setDarkMode] = useState(false);

  return (
    <DarkModeContext.Provider value={{ darkMode, setDarkMode }}>
      {children}
    </DarkModeContext.Provider>
  );
};

// Custom hook to access the dark mode state and setter
export const useDarkMode = () => {
  return useContext(DarkModeContext);
};

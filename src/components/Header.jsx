import React, { useState } from "react";
import { Link } from "react-scroll";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/solid";
import { BsFillMoonStarsFill, BsSun } from "react-icons/bs";
import { useDarkMode } from "../shared/DarkMode";

const Header = () => {
  const { darkMode, setDarkMode } = useDarkMode();
  const [open, setOpen] = useState(false);
  let Links = [
    { name: "Home", link: "home" },
    { name: "About me", link: "about" },
    { name: "Skills", link: "skills" },
    { name: "Projects", link: "projects" },
  ];

  return (
    <div className={darkMode ? "dark" : ""}>
      <div className="w-full relative z-[100]">
        <div className="md:flex items-center justify-between bg-white dark:bg-black py-4 md:py-0 md:px-14 px-7 fixed top-0 left-0 right-0 shadow-sm">
          <div className="flex items-center gap-1">
            {/* dark mode/light mode toggle */}
            {darkMode ? (
              <BsSun
                onClick={() => setDarkMode(!darkMode)}
                className="cursor-pointer md:w-5 md:h-5 w-5 h-5 text-primary dark:text-white"
              />
            ) : (
              <BsFillMoonStarsFill
                onClick={() => setDarkMode(!darkMode)}
                className="cursor-pointer md:w-5 md:h-5 w-5 h-5 text-primary dark:text-white"
              />
            )}
          </div>

          <div
            onClick={() => setOpen(!open)}
            className="absolute right-8 top-6 cursor-pointer md:hidden w-7 h-7 text-primary dark:text-white"
          >
            {open ? <XMarkIcon /> : <Bars3Icon />}
          </div>

          <ul
            className={`md:flex md:items-center items-center md:pb-0 absolute md:static md:z-auto z-50 left-0 w-full md:w-auto mt-4 md:mt-0 md:pl-0 bg-white sm:bg-transparent dark:bg-black ${
              open ? "top-12" : "top-[-490px]"
            }`}
          >
            {Links.map((item) => (
              <li
                key={item.name}
                className="md:ml-8 md:my-8 mt-5 font-semibold text-md border-b-2
              md:border-b-0 text-right px-7 md:px-4 pb-4 md:pb-0 cursor-pointer"
              >
                <Link
                  to={item.link}
                  activeClass="active"
                  smooth={true}
                  spy={true}
                  offset={-100}
                  className="text-light hover:text-secondary dark:text-white duration-700"
                >
                  {item.name}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Header;

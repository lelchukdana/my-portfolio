/* eslint-disable no-unused-vars */
import React from 'react';
import Headline from '../shared/Headline';
import { useDarkMode } from "../shared/DarkMode";
import { FaGraduationCap } from 'react-icons/fa'
// import Skills from './Skills';
// import Education from '../assets/images/education.png'



const About = () => {
    const { darkMode } = useDarkMode();
    return (
        <div className={`max-w-7xl mx-auto my-14 md:py-8 px-7 ${darkMode ? "dark bg-black" : ""}`} id="about">
            <div className='dark:text-white'>
            <Headline title={'ABOUT ME'} />
            </div>
            {/* About Content */}
            <div className='flex flex-col md:flex-row items-start justify-start'>
                {/* left side of the About Me Content */}
                <div className='md:w-1/2 my-8'>
                    <h4 className='text-2xl rubber-duck-font mb-8 dark:text-gray-300'>Get to know me!</h4>
                    <div className='md:w-10/12 text-lg text-[#666] mb-8 dark:text-gray-200'>
                    <p className='mb-5'>I'm a <strong className='text-purple-400'>Full-Stack Developer</strong> proficient in developing web applications, data visualization,
                    and database management.</p>
                    <p className='mb-5'>I found my passion for software engineering during an introductory course to Computer Science
                    in college and I decided to pursue it full-time by enrolling into Hack Reactor. Since then, I have
                    spent my time working on personal projects to continue my learning. Check out some of the work I have
                    done in the <strong><a className='hover:text-purple-400 underline text-' href="#projects">projects</a></strong> section.</p>
                    <p className='mb-5'>I am always eager to expand my skill set and learn new technologies to drive innovation. Currently,
                    I'm open to job opportunities where I can contribute to meaningful projects and collaborate with
                    diverse teams. I'm particularly interested in front-end and back-end development, where I can
                    leverage my expertise in Python, JavaScript, React, and other modern tools and frameworks. If you
                    have any exciting opportunities that align with my interests and values,
                    I would love to <strong><a className='hover:text-purple-400 underline text-' href="https://www.linkedin.com/in/danalelchuk/" target="_blank" rel="noreferrer">connect</a></strong> and explore how I can add value to your organization</p>
                    </div>
                </div>
                {/* right side of the About Me Content */}
                <div className='md:w-1/2 my-8'>
                    <h4 className='text-2xl rubber-duck-font dark:text-gray-300'>My Hobbies</h4>
                    <p className='text-[#666] mb-5 dark:text-gray-200'> I absolutely love coding, but I do manage to pry my eyes away from the screen once in a while,
                    and when I do I enjoy walking my pup, baking, cooking, video-gaming, trying out new restaurants and playing tennis! So if you're looking for a developer who can rally on the court and in code, I'm your match!</p>
                    <div className='flex flex-wrap md:w-10/12 text-center justify-between'>
                    <div className='mb-8'>
                        <img width="52" height="52" src="https://img.icons8.com/cotton/64/dog--v1.png" alt="walking my dog" className='mb-4'/>
                    </div>
                    <div className='mb-8'>
                        <img width="52" height="52" src="https://img.icons8.com/external-colours-bomsymbols-/91/external-baking-furniture-household-colors-colours-bomsymbols-.png" alt="baking" className='mb-4'/>
                    </div>
                    {/* this one below */}
                    <div className='mb-8'>
                        <img width="52" height="52" src="https://img.icons8.com/external-freebies-bomsymbols-/91/external-chopsticks-food-set-3-freebies-bomsymbols-.png" alt="takeout" className='mb-4'/>
                    </div>
                    <div className='mb-8'>
                        <img width="52" height="52" src="https://img.icons8.com/cotton/64/tennis-ball--v1.png" alt="tennis ball"/>
                    </div>
                    <div className='mb-8'>
                        <img width="52" height="52" src="https://img.icons8.com/cotton/64/controller--v3.png" alt="video game controller"/>
                    </div>
                    </div>
                    <h4 className='my-10 text-2xl rubber-duck-font mb-8 dark:text-gray-300'>My education</h4>
                    <div className='md:w-10/12 text-lg text-[#666] mb-2 dark:text-gray-200 flex items-start'>
                    <div className='flex items-center'>
                    <FaGraduationCap className='text-black dark:text-purple-400'/>
                    <p className='ml-2'> DePaul University </p>
                    </div>
                    </div>
                    <div className='md:w-10/12 text-lg text-[#666] dark:text-gray-200 flex items-start'>
                    <div className='flex items-center'>
                    <FaGraduationCap className='text-black dark:text-purple-400'/>
                    <p className='ml-2'> Galvanize Hack Reactor </p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default About;

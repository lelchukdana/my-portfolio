/* eslint-disable no-unused-vars */
import React from 'react';
import { motion } from 'framer-motion';
import { fadeIn } from '../variants';
import profilepic from '../assets/images/ProfilePicPortfolio.png';
import { useDarkMode } from "../shared/DarkMode";
import { RiNewspaperLine } from 'react-icons/ri';
import resume from '../assets/resume/resume.pdf';
import { GoMail } from 'react-icons/go'


const Home = () => {
  const { darkMode } = useDarkMode();

  return (
    <div className={`h-screen flex items-center justify-center ${darkMode ? "dark bg-black" : ""}`} id='home'>
      <motion.div className='text-center md:w-1/2 flex flex-col items-center'
      variants={fadeIn("up", 0.3)}
      initial="hidden"
      whileInView={"show"}
      viewport={{once:false, amount: 0.7}}
      >
        <div className='mb-4 relative md:w-1/2'>
          <img src={profilepic} alt="Profile"/>
        </div>
        <h1 className='text-primary text-7xl rubber-duck-font tracking-wider mb-2 dark:text-white'>Hi, I'm Dana!</h1>
        <p className='text-light text-2xl my-4 dark:text-white'>
          A <strong className='text-purple-400'>Full-Stack Developer</strong> based in Chicago, IL.
        </p>
        <div className='text-center flex mb-4 justify-center'>
        <a
          href={resume}
          download={resume}
          title="Download my Resume"
          target="_blank"
          rel="noopener noreferrer"
          className="project-icon py-4 px-3 flex text-left items-center gap-2 dark:text-white dark:bg-black"
          >
          <RiNewspaperLine size={40} />
          <strong>Download my Resume</strong>
        </a>
        <a
          href="mailto:lelchukdana@gmail.com"
          title="Email Me"
          target="_blank"
          rel="noopener noreferrer"
          className="project-icon py-4 px-4 flex text-left items-center gap-2 dark:text-white dark:bg-black"
          >
          <GoMail size={40} className='dark:text-white' />
          <strong>Contact me</strong>
        </a>
        </div>
        </motion.div>
    </div>
  );
};

export default Home;

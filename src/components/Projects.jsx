/* eslint-disable no-unused-vars */
import React from 'react';
import Headline from '../shared/Headline';
import projects from '../shared/ProjectList';
import { AiOutlineGitlab } from 'react-icons/ai'
import { useDarkMode } from "../shared/DarkMode";
import {BiLogoMongodb, BiLogoReact, BiLogoDjango, BiLogoBootstrap} from 'react-icons/bi'
import {SiFastapi, SiTailwindcss, SiRedux} from 'react-icons/si'
import {BsFiletypeHtml, BsFiletypeCss} from 'react-icons/bs'




const Projects = () => {
    const { darkMode } = useDarkMode();
    return (
        <div className='max-w-7xl mx-auto md:py-8 px-7 dark:text-white' id="projects">
            <Headline title={'PROJECTS'} subtitle={'Here are some of the personal projects that I created'}/>
        <div>
            {
                projects.map( project => <div key={project.id}>
                    <div className='projects'>
                        <div className='rounded-md'>
                            <a href={project.link}>
                        <img src={darkMode ? project.imageDarkMode : project.image} alt="project" className='project-icon'/>
                        </a>
                        </div>
                        <div>
                            <h4 className='text-5xl font-bold mb-6 dark:text-white'>{project.name}</h4>
                            <div className='flex items-center mb-4'>
                            {project.id === 1 && (
                                <div className='flex items-center gap-4'>
                                <BiLogoMongodb size={30} />
                                <SiFastapi size={25} />
                                <SiRedux size={25}/>
                                <BiLogoReact size={30} />
                                <SiTailwindcss size={25}/>
                                </div>
                            )}
                            {project.id === 2 && (
                            <div className='flex items-center gap-4'>
                                <BiLogoDjango size={30}/>
                                <BiLogoReact size={30} />
                                <BiLogoBootstrap size={30} />
                            </div>
                            )}
                            {project.id === 3 && (
                            <div className='flex items-center gap-4'>
                                <BsFiletypeCss size={25}/>
                                <BsFiletypeHtml size={25} />
                                <BiLogoReact size={30} />
                            </div>
                            )}
                        </div>
                            <p className='text-lg text-[#666] leading-6 mb-6 dark:text-gray-300'>{project.description}</p>
                            <a
                                href={project.link}
                                target="_blank"
                                rel="noopener noreferrer"
                                className="project-icon py-4 px-1 flex text-left items-center gap-2 dark:text-white"
                            >
                                <AiOutlineGitlab size={40} />
                            </a>
                        </div>
                    </div>
                </div>
                )
            }
        </div>
    </div>
    );
};

export default Projects;

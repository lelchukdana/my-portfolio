import React from 'react';
import html from '../assets/skills/html.svg';
import css from '../assets/skills/css.svg';
import javascript from '../assets/skills/javascript.svg';
import python from '../assets/skills/python.svg';
import reactSVG from '../assets/skills/reactsvg.svg';
import bootstrap from '../assets/skills/bootstrap.svg';
import tailwind from '../assets/skills/tailwind.svg';
import redux from '../assets/skills/redux.svg';
import mongodb from '../assets/skills/mongodb.svg';
import postgresql from '../assets/skills/postgresql.svg';
import django from '../assets/skills/django.svg';
import docker from '../assets/skills/docker.svg';
import Headline from '../shared/Headline';

const SkillsPage = () => {
  return (
    <div id="skills">
        <div className='dark:text-white'>
            <Headline title={'My Skills'} subtitle={
                'Here you will find more information about me, what I do, and my current skill set'
            }/>
      </div>
      <div className='flex justify-center'>
        <img src={html} alt="HTML5" className='w-13 h-13 m-5 project-icon'/>
        <img src={css} alt="CSS" className='w-13 h-13 m-5 project-icon'/>
        <img src={javascript} alt="Javascript" className='w-13 h-13 m-5 project-icon'/>
        <img src={python} alt="Python" className='w-13 h-13 m-5 project-icon'/>
      </div>
      <div className='flex justify-center'>
        <img src={reactSVG} alt="React" className='w-13 h-13 m-5 project-icon'/>
        <img src={bootstrap} alt="Bootstrap" className='w-13 h-13 m-5 project-icon'/>
        <img src={tailwind} alt="TailwindCSS" className='w-13 h-13 m-5 project-icon'/>
        <img src={redux} alt="Redux" className='w-13 h-13 m-5 project-icon'/>
      </div>
      <div className='flex justify-center'>
        <img src={mongodb} alt="MongoDB" className='w-13 h-13 m-5 project-icon'/>
        <img src={postgresql} alt="PostgreSQL" className='w-13 h-13 m-5 project-icon'/>
        <img src={django} alt="Django" className='w-13 h-13 m-5 project-icon'/>
        <img src={docker} alt="Docker" className='w-13 h-13 m-5 project-icon'/>
      </div>
    </div>
  );
};

export default SkillsPage;

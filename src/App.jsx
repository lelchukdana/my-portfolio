import './App.css';
import Header from './components/Header';
import Home from './components/Home';
import About from './components/About';
import Projects from './components/Projects';
import Footer from './shared/Footer';
import { useDarkMode } from "./shared/DarkMode";
import SkillsPage from './components/SkillsPage';

function App() {
  const { darkMode } = useDarkMode();
  return (
    <>
    <div className="App">
      <div className={`${darkMode ? "dark bg-black" : ""}`}>
        <Header />
        <Home />
        <About />
        <SkillsPage />
        <Projects />
        <Footer />
      </div>
    </div>
    </>
  );
}

export default App;
